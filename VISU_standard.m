% test and validation
clc
clear all
close all
%% INPUT
addpath('fonctions')
tic

cgsd = 0;

% LOAD HEIGHT

writeIm=1 % option to write image


Color_map = colorcube(17);
datasetTable =  ;


nameIm_1 = 
    
boxPy = csvread([dataset '\' nameIm_1 '_box_' num2str((imodel1)) '.csv']);
scorePy = single(csvread([dataset '\' nameIm_1 '_Score_' num2str((imodel1)) '.csv']));
Indice = csvread([dataset '\' nameIm_1 '_Indice_' num2str((imodel1)) '.csv']);
% filtering
 disp('fitlering box')
[boxPy,scorePy,~] = filteringBox_nobord(boxPy,scorePy,Indice,0.5,0.915);

                        label_str1 = cell(size(boxPy,1),1);
                        for i=1:size(boxPy,1)
                            label_str1{i} = num2str(scorePy(i));
                        end

img = imread([datasetImg '\' nameIm_1 '.JPG']);

disp('insert box in the image')
img = insertShape(img,'FilledRectangle',GroundTruth_1_1,'Color', colorT(30,:)*255,'Opacity',0.32);
img = insertObjectAnnotation(img, 'rectangle', GroundTruth_1_1,label_str1,'LineWidth',9,'FontSize',20,'Color','yellow');
disp('write image')
imwrite(img(:,:,:),['D:\Home\SimonMADEC\forHiphen\visu\' nameIm_1 '_' num2str(imodel1) '_newPara.jpg'])
    




