function [boxPy,scorePy,Indice] = filteringBox_nobord(boxPy,scorePy,Indice,threshold,ovThresold)
%UNTITLED8 Summary of this function goes here
%   Detailed explanation goes here
if nargin<4
    threshold=0.5;
end
    
if nargin<5
    ovThresold=0.8;
end
            boxPy = round(boxPy);
            boxPy(:,3) = boxPy(:,3)-boxPy(:,1);
            boxPy(:,4) = boxPy(:,4)-boxPy(:,2);
            boxPyOld = boxPy;
            boxPy(:,1) = boxPyOld(:,2);
            boxPy(:,2) = boxPyOld(:,1);
            boxPy(:,3) = boxPyOld(:,4);
            boxPy(:,4) = boxPyOld(:,3);
            
           cond1 =  (boxPy(:,1) < 20);
           cond2 =  (boxPy(:,1) + boxPy(:,3)  > 5980);
           cond3 = (boxPy(:,2) < 20);
           cond4 = (boxPy(:,2) + boxPy(:,4)  > 3980);
           
           cond = [cond1,cond2,cond3,cond4];
    
           vec = sum(cond')';
            
            boxPyA = boxPy((vec==1),:);
            scorePyA = scorePy( (vec==1),:);
                       

            boxPy = boxPy((Indice==0) & (vec==0),:);
            scorePy = scorePy((Indice==0) & (vec==0),:);
            
            boxPy = [boxPy;boxPyA];
            scorePy = [scorePy;scorePyA];
            
            boxPy = boxPy(scorePy>threshold,:);
            scorePy = scorePy(scorePy>threshold); 
           

            if size(scorePy,1)>1
                [boxPy,scorePy] = selectStrongestBbox(boxPy,scorePy,'RatioType','Min','OverlapThreshold',ovThresold); % #todo 0.73 !! #TODO    
            end

end

