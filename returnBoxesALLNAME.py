# -*- coding: utf-8 -*-
"""
Created on Wed Jun 20 17:09:33 2018

@author: simadec
"""

# -*- coding: utf-8 -*-
"""
This function return all the CSV score and bounding box along with some indices like if the boundix box is have some border with the borders of the images. 
Not that this function crop the 6000x4000 images by 1000x1000 with a 50% overlap
"""


 
def returnBound(MODEL_NAME,pathIm):
    # coding: utf-8
    import numpy as np
    import tensorflow as tf
    import glob
    from PIL import Image

    # # Model preparation 
    # Path to frozen detection graph. This is the actual model that is used for the object detection. lalalalalalla

    detection_graph = tf.Graph()
    with detection_graph.as_default():
      od_graph_def = tf.GraphDef()
      print('coucou on va lire le model')
      with tf.gfile.GFile(MODEL_NAME, 'rb') as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name='')
        
    ## Loading label map
    def load_image_into_numpy_array(image):
      (im_width, im_height) = image.size
      return np.array(image.getdata()).reshape(
          (im_height, im_width, 3)).astype(np.uint8)
    print('coucou line 31 on va ouvrir une session tensorlfow')
    with detection_graph.as_default():
      with tf.Session(graph=detection_graph) as sess:
        # Definite input and output Tensors for detection_graph
        image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
        # Each box represents a part of the image where a particular object was detected.
        detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
        # Each score represent how level of confidence for each of the objects.
        # Score is shown on the result image, together with the class label.
        detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
        num_detections = detection_graph.get_tensor_by_name('num_detections:0')
        detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
        


        for image_path in glob.glob(pathIm + '/*.JPG'):
          print(image_path)
          temp = image_path.split('\\')
          temp = temp[len(temp)-1]
          temp = temp.split('.')
          temp = temp[0]
          numIm = temp
          
             
          print(temp)
          


                  
          
          image = Image.open(image_path)
          # the array based representation of the image will be used later in order to prepare the
          # result image with boxes and labels on it.
          image_np = load_image_into_numpy_array(image)
          
          scoresCumul = []
          boxesCumul= []
          indCumul = []
          

      # ici ca va depéndre de la dataset ...
          rangeI = [500,1000,1500,2000,2500,3000,3500,4000,4500,5000,5500]
          rangeJ = [500,1000,1500,2000,2500,3000,3500]
          sizeImPer2 = 500
              
              
          for i in rangeI: # loop de 1 à 6 pour parce que 1 à 6000 ...
              for j in rangeJ:
                              image_np1 = image_np[j-sizeImPer2:j+sizeImPer2,i-sizeImPer2:i+sizeImPer2]
                              # Expand dimensions since the model expects images to have shape: [1, None, None, 3] 
                              image_np_expanded = np.expand_dims(image_np1, axis=0)
                              # Actual detection.
                              (boxes, scores, classes, num) = sess.run(
                                  [detection_boxes, detection_scores, detection_classes, num_detections],
                                  feed_dict={image_tensor: image_np_expanded})
                              # Visualization of the results of a detection.
                              ind = scores>0 
                              scores = scores[ind]
                              boxes = boxes[ind,0:4]
                              #probleme c nul
                              indiceO = np.zeros(len(boxes)) 
                              print('num epis dans un patch : {}'.format(len(boxes)))
                              for ii in range(0,len(boxes)):
                                  if (boxes[ii,0]<0.01) or (boxes[ii,1]<0.01) or (boxes[ii,2]>0.99) or (boxes[ii,3]>0.99) :
                                      indiceO[ii] = 1
                                  else:
                                      indiceO[ii] = 0      
                              
                              boxes[:,0] = (boxes[:,0]*sizeImPer2*2 +j-sizeImPer2)/4000 #ou moins 1000
                              boxes[:,1] = (boxes[:,1]*sizeImPer2*2 +i-sizeImPer2)/6000
                              boxes[:,2] = (boxes[:,2]*sizeImPer2*2 +j-sizeImPer2)/4000
                              boxes[:,3] = (boxes[:,3]*sizeImPer2*2 +i-sizeImPer2)/6000
                              
                              scoresCumul = np.concatenate((scoresCumul,scores),axis=0)
                              indCumul = np.concatenate((indCumul,indiceO),axis=0)
                              if len(boxesCumul)==0:
                                  boxesCumul = boxes
                                  #print('length box null, carefull if step > 1000')
                              else:
                                  boxesCumul = np.concatenate((boxesCumul,boxes),axis=0)
       
          boxesCumul[:,0]=boxesCumul[:,0]*4000
          boxesCumul[:,1]=boxesCumul[:,1]*6000
          boxesCumul[:,2]=boxesCumul[:,2]*4000
          boxesCumul[:,3]=boxesCumul[:,3]*6000
          
          temp = MODEL_NAME.split('_')
          temp = temp[len(temp)-1]
          temp = temp.split('.')
          temp = temp[0]
          
          print('Step ' + temp)
          np.savetxt("output/{}_box_{}.csv".format(numIm,temp), boxesCumul, delimiter=",")
          np.savetxt("output/{}_Score_{}.csv".format(numIm,temp), scoresCumul, delimiter=",")
          np.savetxt("output/{}_Indice_{}.csv".format(numIm,temp), indCumul, delimiter=",")

